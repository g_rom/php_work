<?php

use yii\db\Migration;

class m161130_192611_base_insert_data extends Migration {
    public function up () {
        $this->batchInsert ('product', ['title', 'price', 'image', 'description', 'count'], [
            ['товар 1', '1000', '/images/item_1.jpg', 'очень хороший товар', '10'],
            ['товар 2', '1500', '/images/item_2.jpg', 'очень хороший товар', '5'],
            ['товар 3', '800', '/images/item_3.jpg', 'очень хороший товар', '30'],
            ['товар 4', '4000', '/images/item_4.jpg', 'очень хороший товар', '1'],
            ['товар 5', '2700', '/images/item_5.jpg', 'очень хороший товар', '15'],
            ['товар 6', '3050', '/images/item_6.jpg', 'очень хороший товар', '7'],
            ['товар 7', '5000', '/images/item_7.jpg', 'очень хороший товар', '100'],
            ['товар 8', '2400', '/images/item_8.jpg', 'очень хороший товар', '13'],
            ['товар 9', '3200', '/images/item_9.jpg', 'очень хороший товар', '12'],
            ['товар 10', '300', '/images/item_10.jpg', 'очень хороший товар', '8'],
            ['товар 11', '4280', '/images/item_11.jpg', 'очень хороший товар', '6'],
            ['товар 12', '2700', '/images/item_12.jpg', 'очень хороший товар', '4'],
            ['товар 13', '3090', '/images/item_13.jpg', 'очень хороший товар', '18'],
            ['товар 14', '3990', '/images/item_14.jpg', 'очень хороший товар', '22'],
        ]);
    }

    public function down () {
//        echo "m161130_192611_base_insert_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
