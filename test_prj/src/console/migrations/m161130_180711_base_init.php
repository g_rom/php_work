<?php

use yii\db\Migration;

class m161130_180711_base_init extends Migration {
    public function up () {
        $this->createTable ('product', [
            'id' => $this->primaryKey (),
            'title' => $this->string ()->notNull (),
            'price' => $this->integer (),
            'image' => $this->string (),
            'description' => $this->text (),
            'count' => $this->integer ()
        ]);
        $this->createIndex (
            'idx-product-id',
            'product',
            'id'
        );

        $this->createTable ('orders', [
            'id' => $this->primaryKey (),
            'total_price' => $this->integer ()
        ]);
        $this->createIndex (
            'idx-orders-id',
            'orders',
            'id'
        );
        $this->createTable ('order_product', [
            'id' => $this->primaryKey (),
            'order_number' => $this->integer ()->notNull (),
            'good_id' => $this->integer ()->notNull (),
            'count' => $this->integer ()
        ]);
        $this->createIndex (
            'idx-order_product-id',
            'order_product',
            'id'
        );
        $this->addForeignKey (
            'fk-order_product-order_number',
            'order_product',
            'order_number',
            'orders',
            'id',
            'RESTRICT'
        );
        $this->addForeignKey (
            'fk-order_product-good_id',
            'order_product',
            'good_id',
            'product',
            'id',
            'RESTRICT'
        );
    }

    public function down () {
        $this->dropTable ('order_product');
        $this->dropTable ('orders');
        $this->dropTable ('product');
        $this->dropIndex (
            'idx-shop-id',
            'product'
        );
        $this->dropIndex (
            'idx-orders-id',
            'orders'
        );
        $this->dropIndex (
            'idx-order_product-id',
            'order_product'
        );
        $this->dropForeignKey (
            'fk-order_product-order_number',
            'order_product'
        );
        $this->dropForeignKey (
            'fk-order_product-good_id',
            'order_product'
        );

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
