var app = require('express')();
var http = require('http');
var request = require('request');
var server = http.Server(app);
var io = require('socket.io')(server);

server.listen(3000);

io.on('connection', function (socket) {
    socket.on("get:products", function (data) {
        var url = 'http://test.prj/site/product';
        if (data !== undefined) {
            data=JSON.parse(data);
            try {
                if ("list" in data && data.list.length > 0) {
                    url += '?productsList=' + JSON.stringify(data.list);
                }
            }
            catch (e) {
            }
            try {
                if ("page" in data) {
                    url += '?pageOffset=' + JSON.stringify(data.page);
                }
            } catch (e) {
            }
        }
        request(url, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                // console.log(body);
                var products = JSON.parse(body);
                socket.emit("products", products);
            }
        });
    });

    socket.on("get:total", function (data) {
        var url = 'http://test.prj/site/product';
        if (data !== undefined && 'list' in data && data.list.length > 0) {
            url += '?productsList=' + JSON.stringify(data.list);
            url += '&getTotal=true';
        }
        request(url, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                // console.log(body);
                var total = JSON.parse(body);
                socket.emit("total", total);
            }
        });
    });

    socket.on("order:confirm", function (data) {
        var url = 'http://test.prj/cart/confirm-order';
        if (data !== undefined && 'list' in data && data.list.length > 0) {
            url += '?productsList=' + JSON.stringify(data.list);
        }
        request(url, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                // console.log(body);
                var total = JSON.parse(body);
                socket.emit("order", total);
            }
        });
    });

    socket.on('disconnect', function () {
        // console.log('user disconnected');
    });
});