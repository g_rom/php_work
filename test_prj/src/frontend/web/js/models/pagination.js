App.Models.Pagination = Backbone.Collection.extend({
    model: function (options) {
        return new App.Models.PaginationLink(options);
    }
});
App.Models.PaginationLink = Backbone.Model.extend({
    default: {
        active: false,
        page_count: 0
    },
    initialize: function (options) {
        this.active = options.active;
        this.page_count = options.page_count;
    },
    setAttr: function (key, value) {
        this[key] = value;
        this.set(key, value);
    }
});