App.Models.Products = Backbone.Collection.extend({
    model: function (options) {
        return new App.Models.Product(options);
    }
});
App.Models.Product = Backbone.Model.extend({
    default: {
        image: '#',
        title: 'no name',
        id: 0,
        price: 0,
        count: 0,
        cart_count: 0
    },
    initialize: function (options) {
        this.image = options.image;
        this.title = options.title;
        this.id = options.id;
        this.price = options.price;
        this.count = options.count;
        if (this.count > 0) {
            this.show = true;
            this.text = "В корзину";
        }
    },
    setAttr: function (key, value) {
        this[key] = value;
        this.set(key, value);
    }
});
App.Models.ProductBtn = Backbone.Model.extend({
    default: {
        show: false,
        text: 'Нет в наличии',
        title: 'no name',
        id: 0,
        price: 0,
        count: 0
    },
    initialize: function (options) {
        this.show = options.show;
        this.text = options.text;
        this.title = options.title;
        this.id = options.id;
        this.price = options.price;
        this.count = options.count;
    }
});