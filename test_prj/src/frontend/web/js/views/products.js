var itemId = 0;

App.Views.Products = Backbone.View.extend({
    tagName: 'div',
    className: "row",
    initialize: function () {
        this.collection.on('add', this.addOne, this);
    },
    render: function () {
        this.$el.empty();
        this.collection.each(this.addOne, this);
        return this;
    },
    addOne: function (model) {
        var view = new App.Views.Product({model: model});
        this.$el.append(view.render().el);
    }
});
App.Views.Pagination = Backbone.View.extend({
    tagName: "div",
    className: "col-sm-12 text-center",
    render: function () {
        var links = new App.Views.PaginationLinks({model: this.collection.models});
        this.$el.html(links.render().el);
        return this;
    }
});
App.Views.PaginationLinks = Backbone.View.extend({
    tagName: "ul",
    className: "pagination",
    render: function () {
        for (var i = 0; i < this.model[0].page_count; i++) {
            this.addLinks(i);
        }
        return this;
    },
    addLinks: function (number) {
        var link = new App.Views.PaginationLink({model: {id: number, active: false}});
        this.$el.append(link.render().el);
    }
});
App.Views.PaginationLink = Backbone.View.extend({
    tagName: "li",
    events: {
        "click": "setActive"
    },
    render: function () {
        if (this.model.active) {
            this.className = "active";
        }
        this.$el.html('<a href="/?page=' + this.model.id + '">' + (this.model.id + 1) + '</a>');
        return this;
    },
    setActive: function () {
        return this;
    }
});

App.Views.Product = Backbone.View.extend({
    tagName: "div",
    className: "col-sm-6 col-md-3",
    initialize: function () {
        this.model.on('destroy', this.remove, this);
        this.model.on('change', this.changeItem, this);
    },
    render: function () {
        var img = '<img src="' + this.model.image + '" alt="' + this.model.title + '">';
        var btn = new App.Views.ProductData({model: this.model});
        this.$el.html('<div class="thumbnail"></div>');
        this.$el.find(".thumbnail").append(img);
        this.$el.find(".thumbnail").append(btn.render().el);
        return this;
    },
    changeItem: function () {
        var img = '<img src="' + this.model.image + '" alt="' + this.model.title + '">';
        var btn = new App.Views.ProductData({model: this.model});
        this.$el.html('<div class="thumbnail"></div>');
        this.$el.find(".thumbnail").append(img);
        this.$el.find(".thumbnail").append(btn.render().el);
        return this;
    }
});
App.Views.ProductData = Backbone.View.extend({
    tagName: "div",
    className: "caption",
    initialize: function () {
        this.model.on('change', this.changeItem, this);
    },
    render: function () {
        var title = '<h3>' + this.model.title + '</h3>';
        var price = '<p>' + this.model.price + ' руб</p>';
        var btn = new App.Views.ProductBtn({model:this.model});
        this.$el.html(title + price);
        this.$el.append(btn.render().el);
        return this;
    },
    changeItem: function () {
        this.$el.html(this.el);
        return this;
    }
});
App.Views.ProductBtn = Backbone.View.extend({
    tagName: "span",
    events: {
        "click": "setActive"
    },
    initialize: function () {
        this.model.on('change', this.changeItem, this);
    },
    render: function () {
        itemId = this.model.id;
        var setBtn = true;
        if (getCookie("cart_items") !== undefined) {
            var cart_items = JSON.parse(getCookie("cart_items"));
            var itemIndex = cart_items.findIndex(findItemIndex);
            if (itemIndex >= 0) {
                setBtn = false;
            }
        }
        if (this.model.count > 0) {
            if (setBtn) {
                var btn = '<button class="btn add-to-cart btn-primary" data-product="' + this.model.id + '">' + this.model.text + '</button>';
            } else {
                var btn = '<a href="/cart" class="btn btn-success">В корзине</a>';
            }
        } else {
            var btn = '<p class="text-muted">' + this.model.text + '</p>';
        }
        this.$el.html(btn);
        return this;
    },
    changeItem: function () {
        this.$el.html(this.el);
        return this;
    },
    setActive: function () {
        itemId = this.model.id;
        if (getCookie("cart_items") === undefined) {
            var cart_items = [];
            cart_items[0] = {id: itemId, count: 1};
        } else {
            var cart_items = JSON.parse(getCookie("cart_items"));
            var itemIndex = cart_items.findIndex(findItemIndex);
            if (itemIndex < 0) {
                cart_items[cart_items.length] = {id: itemId, count: 1};
            }
        }
        setCookie("cart_items", JSON.stringify(cart_items), coockOpt);
        var btn = '<a href="/cart" class="btn btn-success">В корзине</a>';

        this.$el.html(btn);
        return this;
    }
});