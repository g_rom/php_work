var itemId = 0;

App.Views.CartProducts = Backbone.View.extend({
    tagName: 'table',
    className: "table table-striped table-hover",
    initialize: function () {
        this.collection.on('add', this.addOne, this);
    },
    render: function () {
        this.$el.empty();
        if (getCookie("cart_items") !== undefined) {
            var cart_items = JSON.parse(getCookie("cart_items"));
            if (cart_items.length > 0) {
                this.addAll();
            }
        }
        return this;
    },
    addOne: function (model) {
        var view = new App.Views.CartProduct({model: model, collection: this.collection});
        this.$el.append(view.render().el);
        if (model.cid == _.last(this.collection.models).cid) {
            this.addTotal();
        }
    },
    addAll: function () {
        this.collection.each(this.addOne, this);
        return this;
    },
    addTotal: function () {
        var view = new App.Views.CartTotal({collection: this.collection});
        this.$el.append(view.render().el);
        return this;
    }
});

App.Views.CartProduct = Backbone.View.extend({
    tagName: "tr",
    initialize: function () {
        this.model.on('destroy', this.remove, this);
        var count = 0;
        itemId = this.model.id;
        if (getCookie("cart_items") !== undefined) {
            var cart_items = JSON.parse(getCookie("cart_items"));
            var itemIndex = cart_items.findIndex(findItemIndex);
            if (itemIndex >= 0) {
                count = cart_items[itemIndex].count;
            }
        }
        this.model.cart_count = count;
    },
    render: function () {
        var img = new App.Views.CartProductImg({model: this.model});
        var title = new App.Views.CartProductName({model: this.model});
        var price = new App.Views.CartProductPrice({model: this.model});
        var count = new App.Views.CartProductCount({model: this.model});
        var counter = new App.Views.CartProductCounter({model: this.model});
        var button = new App.Views.CartProductButton({model: this.model, collection: this.collection});

        this.$el.append(img.render().el);
        this.$el.append(title.render().el);
        this.$el.append(price.render().el);
        this.$el.append(count.render().el);
        this.$el.append(counter.render().el);
        this.$el.append(button.render().el);
        return this;
    }
});

App.Views.CartProductImg = Backbone.View.extend({
    tagName: "td",
    className: "cart-image",
    initialize: function () {
        this.model.on('change', this.changeItem, this);
    },
    render: function () {
        var img = '<img src="' + this.model.image + '" alt="' + this.model.title + '" class="img-thumbnail">';
        this.$el.html(img);
        return this;
    },
    changeItem: function () {
        this.render();
        return this;
    }
});
App.Views.CartProductName = Backbone.View.extend({
    tagName: "td",
    className: "text-left",
    initialize: function () {
        this.model.on('change', this.changeItem, this);
    },
    render: function () {
        this.$el.html(this.model.title);
        return this;
    },
    changeItem: function () {
        this.render();
        return this;
    }
});
App.Views.CartProductPrice = Backbone.View.extend({
    tagName: "td",
    className: "price",
    initialize: function () {
        this.model.on('change', this.changeItem, this);
    },
    render: function () {
        this.$el.html(this.model.price);
        return this;
    },
    changeItem: function () {
        this.render();
        return this;
    }
});
App.Views.CartProductCount = Backbone.View.extend({
    tagName: "td",
    className: "price",
    initialize: function () {
        this.model.on('change', this.changeItem, this);
    },
    render: function () {
        this.$el.html(this.model.cart_count);
        return this;
    },
    changeItem: function () {
        this.render();
        return this;
    }
});
App.Views.CartProductCounter = Backbone.View.extend({
    tagName: "td",
    className: "price",
    initialize: function () {
        this.model.on('change', this.changeItem, this);
    },
    render: function () {
        var field = new App.Views.CartProductCounterField({model: this.model});
        var block = '<div class="form-group"></div>';

        this.$el.html(block);
        this.$el.find(".form-group").append(field.render().el);
        return this;
    },
    changeItem: function () {
        this.render();
        return this;
    }
});
App.Views.CartProductCounterField = Backbone.View.extend({
    tagName: "div",
    className: "input-group product-count",
    events: {
        "change": "changeCount"
    },
    initialize: function () {
        this.model.on('change', this.changeItem, this);
    },
    render: function () {
        var minusBtn = new App.Views.CartProductCounterBtn({model: this.model, step: -1});
        var plusBtn = new App.Views.CartProductCounterBtn({model: this.model, step: 1});

        if (this.model.cart_count > 1) {
            this.$el.append(minusBtn.render().el);
        }
        this.$el.append('<input type="text" class="form-control text-center" value="' + this.model.cart_count + '">');
        if (this.model.cart_count < this.model.count) {
            this.$el.append(plusBtn.render().el);
        }
        return this;
    },
    changeItem: function () {
        this.render();
        return this;
    },
    changeCount: function () {
        var value = this.$el.find(".form-control").val();
        if (value < 1) {
            value = 1;
        }
        if (value > this.model.count) {
            value = this.model.count;
        }

        itemId = this.model.id;
        if (getCookie("cart_items") !== undefined) {
            var cart_items = JSON.parse(getCookie("cart_items"));
            var itemIndex = cart_items.findIndex(findItemIndex);
            if (itemIndex >= 0) {
                cart_items[itemIndex] = {id: itemId, count: value};
            }
        }
        setCookie("cart_items", JSON.stringify(cart_items), coockOpt);
        this.model.setAttr("cart_count", value);
        return this;
    }
});
App.Views.CartProductCounterBtn = Backbone.View.extend({
    tagName: "a",
    className: "input-group-addon change-count",
    events: {
        "click": "changeCount"
    },
    initialize: function (options) {
        this.step = options.step;
    },
    render: function () {
        if (this.step > 0) {
            var icon = '<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>';
        } else {
            var icon = '<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>';
        }
        this.$el.html(icon);
        return this;
    },
    changeCount: function () {
        itemId = this.model.id;
        if (getCookie("cart_items") !== undefined) {
            var cart_items = JSON.parse(getCookie("cart_items"));
            var itemIndex = cart_items.findIndex(findItemIndex);
            if (itemIndex >= 0) {
                cart_items[itemIndex] = {id: itemId, count: cart_items[itemIndex].count + this.step};
            }
        }
        setCookie("cart_items", JSON.stringify(cart_items), coockOpt);
        this.model.setAttr("cart_count", this.model.cart_count + this.step);
        return this;
    }
});
App.Views.CartProductButton = Backbone.View.extend({
    tagName: "td",
    className: "buttons",
    events: {
        "click": "removeItem"
    },
    render: function () {
        var block = '<button class="btn btn-danger remove-from-cart">Удалить из заказа</button>';
        this.$el.html(block);
        return this;
    },
    removeItem: function () {
        this.model.trigger('destroy');
        itemId = this.model.id;
        if (getCookie("cart_items") !== undefined) {
            var cart_items = JSON.parse(getCookie("cart_items"));
            var itemIndex = cart_items.findIndex(findItemIndex);
            if (itemIndex >= 0) {
                cart_items.splice(itemIndex, 1);
            }
            setCookie("cart_items", JSON.stringify(cart_items), coockOpt);
            this.collection.remove(this.model);
            this.collection.trigger('change');
        }
        return this;
    }
});
App.Views.CartTotal = Backbone.View.extend({
    tagName: "tr",
    initialize: function () {
        this.collection.on('change', this.changeItem, this);
        this.getTotal();
    },
    render: function () {
        var total = '<td colspan="2" class="text-right"><h4>Итого</h4></td><td><h4><span class="total">' + this.total + '</span> руб.</h4></td>';
        var orderConfirm = '<td colspan="2"></td>';
        var confirm = new App.Views.ConfirmButton({collection: this.collection});
        this.$el.html(total + orderConfirm);
        this.$el.append(confirm.render().el);
        return this;
    },
    changeItem: function () {
        if (this.collection.length > 0) {
            this.getTotal();
        } else {
            this.$el.empty();
        }
        return this;
    },
    getTotal: function () {
        var _that = this;
        var items = [];
        _.each(this.collection.models, function (element, index, list) {
            return this[this.length] = {id: element.id, count: element.cart_count};
        }, items);

        socket.emit("get:total", {list: items});
        socket.on("total", function (data) {
            _that.total = data;
            _that.render();
        });
    }
});
App.Views.ConfirmButton = Backbone.View.extend({
    tagName: "td",
    events: {
        "click": "confirm"
    },
    render: function () {
        var block = '<button class="btn btn-primary confirm-order">Подтвердить</button>';
        this.$el.html(block);
        return this;
    },
    confirm: function () {
        var items = [];
        _.each(this.collection.models, function (element, index, list) {
            return this[this.length] = {id: element.id, count: element.cart_count};
        }, items);

        socket.emit("order:confirm", {list: items});
        socket.on("order", function (data) {
            if ('SUCCESS' in data) {
                deleteCookie("cart_items");
                var answer = new App.Views.SuccessOrder({model: data});
                answer.render();
            }
        });
    }
});
App.Views.SuccessOrder = Backbone.View.extend({
    tagName: "div",
    render: function () {
        var block = '<h3 class="text-center">' + this.model.SUCCESS + '</h3>';
        $("#products").html(block);
        return this;
    }
});