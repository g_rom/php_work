var socket = io.connect('http://test.prj:8080');
App.Router = Backbone.Router.extend({
    routes: {
        '(:?page)': 'index',
        'cart(/)': 'cart',
    },
    index: function (page) {
        if (page != null) {
            page = page.split("page=")[1];
        }

        var appProducts = new App.Models.Products();
        var appPagin = new App.Models.Pagination();
        socket.emit("get:products", JSON.stringify({page: parseInt(page)}));
        socket.on("products", function (data) {
            appProducts.add(data.products);
            appPagin.add(data.pagination);

            var ProductsView = new App.Views.Products({collection: appProducts});
            ProductsView.render();
            $("#products").html(ProductsView.el);

            var pagination = new App.Views.Pagination({collection: appPagin, active: page});
            pagination.render();
            $("#products").append(pagination.el);
        });
    },
    cart: function () {
        var appProducts = new App.Models.Products();
        if (getCookie("cart_items") !== undefined) {
            var cart_items = JSON.parse(getCookie("cart_items"));

            var items = [];
            _.each(cart_items, function (element, index, list) {
                return this[this.length] = element.id;
            }, items);

            socket.emit("get:products", JSON.stringify({list: items}));
            socket.on("products", function (data) {
                appProducts.add(data.products);

                var ProductsView = new App.Views.CartProducts({collection: appProducts});
                ProductsView.render();
                $("#products").html(ProductsView.el);
            });
        }
    }
});

new App.Router();
Backbone.history.start({pushState: true, root: "/"});