<?php
/**
 * Created by PhpStorm.
 * User: grom
 * Date: 01/12/2016
 * Time: 01:23
 */

namespace frontend\controllers;

use common\models\Orders;
use common\models\Product;
use Yii;
use yii\web\Controller;

class CartController extends Controller {
    public function actionIndex () {
        return $this->render ('index');
    }

    public function actionConfirmOrder ($productsList = array()) {
        $query = Product::find ();
        $query->orderBy ('price');

        if (!empty($productsList) && !empty(json_decode ($productsList))) {
            $id = [];
            $cartProducts = [];
            foreach (json_decode ($productsList) as $product) {
                $id[] = $product->id;
                $cartProducts['id_' . $product->id] = $product;
            }
            $query->where (['id' => $id]);
        }

        $products = $query->all ();
        $total = $this->getTotalPrice ($cartProducts, $products);

        $answer = array();

        if (!empty($cartProducts)) {
            $order = new Orders();
            $order->total_price = $total;
            $order->save ();
            $order_id = Yii::$app->db->getlastinsertid ();
            $insert_product = array();
            foreach ($cartProducts as $product) {
                $insert_product[] = [$order_id, $product->id, $product->count];
                $query = Product::findOne ($product->id);
                if ($query->count - $product->count >= 0) {
                    $query->updateCounters (['count' => -$product->count]);
                } else {
                    $query->updateCounters (['count' => -$query->count]);
                }
            }

            $db = \Yii::$app->db;
            $db->createCommand ()->batchInsert ('order_product', ['order_number', 'good_id', 'count'], $insert_product)->execute ();

            $answer['SUCCESS'] = 'Товар добавлен';

            Yii::$app->mailer->compose ()
                ->setFrom ('from@domain.com')
                ->setTo ('to@domain.com')
                ->setSubject ('Message subject')
                ->setTextBody ('Plain text content')
                ->setHtmlBody ('<b>HTML content</b>')
                ->send ();

        } else {
            $answer['ERROR'] = 'Что-то не правильно';
        }

        return json_encode ($answer);
    }

    private function getTotalPrice ($cartProducts = array(), $products = array()) {
        $total = 0;
        foreach ($products as &$product) {
            $total += ($product->price * $cartProducts['id_' . $product->id]->count);
        }

        return $total;
    }
}