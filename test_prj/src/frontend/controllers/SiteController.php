<?php
namespace frontend\controllers;

use common\models\Product;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller {
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex () {
        return $this->render ('index');
    }

    public function actionProduct ($productID = 0, $pageOffset = 0, $productsList = array(), $getTotal = false) {
        $query = Product::find ();

        $pagination = new Pagination([
            'defaultPageSize' => 8,
            'totalCount' => $query->count (),
        ]);
        $pagination->setPage (intval ($pageOffset));

        $pageCount = 0;
        $pageSize = $pagination->defaultPageSize;
        if ($pageSize < 1) {
            $pageCount = $pagination->totalCount > 0 ? 1 : 0;
        } else {
            $totalCount = $pagination->totalCount < 0 ? 0 : (int)$pagination->totalCount;

            $pageCount = (int)(($totalCount + $pageSize - 1) / $pageSize);
        }

        $query->orderBy ('price')->offset ($pagination->offset)->limit ($pagination->limit);

        if (!$getTotal) {
            if (!empty($productsList) && !empty(json_decode ($productsList))) {
                $query->where (['id' => json_decode ($productsList)]);
            } elseif (intval ($productID) > 0) {
                $query->where (['id' => intval ($productID)]);
            }
        } else {
            if (!empty($productsList) && !empty(json_decode ($productsList))) {
                $id = [];
                $cartProducts = [];
                foreach (json_decode ($productsList) as $product) {
                    $id[] = $product->id;
                    $cartProducts['id_' . $product->id] = $product;
                }
                $query->where (['id' => $id]);
            }
        }

        $products = $query->all ();

        $answer = array();
        if (!$getTotal) {
            foreach ($products as $product) {
                $answer['products'][] = $product->attributes;
            }
            $answer['pagination']['page_count'] = $pageCount;
            $answer['pagination']['total_count'] = $pagination->totalCount;
        } else {
            $answer = $this->getTotalPrice ($cartProducts, $products);
        }

        return json_encode ($answer);
    }

    private function getTotalPrice ($cartProducts = array(), $products = array()) {
        $total = 0;
        foreach ($products as &$product) {
            $total += ($product->price * $cartProducts['id_' . $product->id]->count);
        }

        return $total;
    }
}
