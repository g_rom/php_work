<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [
        'js/socket.io.js',
        'js/underscore.js',
        'js/backbone.js',
        'js/main.js',
        'js/models/products.js',
        'js/models/pagination.js',
        'js/views/products.js',
        'js/views/cart_products.js',
        'js/app.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
