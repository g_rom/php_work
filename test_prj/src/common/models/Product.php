<?php
/**
 * Created by PhpStorm.
 * User: grom
 * Date: 30/11/2016
 * Time: 22:15
 */

namespace common\models;

use yii\db\ActiveRecord;

class Product extends ActiveRecord {

    public function attributeLabels()
    {
        return array_merge(parent::attributes(),['in_cart']);
    }

    public function attributes()
    {
        return array_merge(parent::attributes(),['in_cart']);
    }
}